console.log("Hello world");

/*
//Mini-Activity
//log in the console your name 20 times
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
console.log("Christopher John Tolang");
*/

//Function
	//Funtions in JavaScript are lines/blocks of code that tell our device/application to perform a certain task when called/invoked

//Function Declarations
	//Function statement defines a function with the specified parameters

//Syntax
/*
		function functionName(){
			code block (statement)
		};
*/

	//function keyword - used to define a JS function
	//functionName - functions are named to be able to use later in the code
	//function block {()} - statements which comprise the body of the function
						//-this is where the code  is to be executed
function printname(){
	console.log("Chris");
}

printname();

//semicolons are used to separate executable JS statement

//function invocation

//call/invoke the functions that we declared
printname();

//printage(); //results in an error, much like variable, we cannot invoke a function we have yet to define

//function declaration vs expression

//function declaration
function declaredFunction(){
	console.log("Hello world from declaredFunction")
};

declaredFunction();

//function expression
//a function can also be stored in a variable. This is called a function variable

//function expression is an anonymous function assigned to the variableFunction

//Anonymous function - a function without a name

let variableFunction = function(){
	console.log("Hello from variableFunction")
};

variableFunction();

//function expression of a function named funcName assigned to the variable funcExpression

//they are ALWAYS invoked (called) using the variable name

let funcExpression = function funcName(){
	console.log("Hello Form the Other Side!!!")
};

funcExpression();

declaredFunction = function(){
	console.log("updated declaredFunction")
};

declaredFunction();

funcExpression = function(){
	console.log("updated funcExpression")
};

funcExpression();

const constantFunc = function() {
	console.log("initialized with const")
};

constantFunc();

/*
constantFunc = function(){
	console.log("Cannot be reassigned")
};

constantFunc();*/ //

/*

*/

{
	let localVar = "Juan Dela Cruz";
}

let globalVar = "Mr. Worldwide"

console.log(globalVar);
//console.log(localVar);//result in error


/*
	JS has a function scope:
	1. 
*/

function showNames(){
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionConst);
	console.log(functionLet)
}

showNames();

//console.log(functionConst); //Uncaught ReferenceError: functionConst is not defined
//console.log(functionLet); //Uncaught ReferenceError: functionConst is not defined

//Nested Functions
		
		//we can create another function inside a function

function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
		//console.log(nestedName);
	}
	//console.log(nestedName); //results in error
	//nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in
	nestedFunction();
}

myNewFunction();//Jane
//nestedFunction();//return an error
//Moreover, since this function is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in

//Function and Global Scoped Variables

	let globalName = "Cardo";

	function myNewFunction2(){
		let nameInside = "Mario";

		console.log(globalName);
		//variables declared globally (outside function) have a global scope
		//global variables can be accessed from anywhere in JS
		//program including from inside a function
	}

	myNewFunction2();//Cardo
	//console.log(nameInside);//error
		//nameInside is a function-scoped. It cannot be accessed globally


//using alert()

	alert("Hello world")

	function showSampleAlert(){
		alert("Hello User! Welcome to B248 Class!")
	}

	showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed. ");

//using prompt()
//the input from the prompt () will be returned as a string once the user dismisses the window

	let samplePrompt = prompt("Enter your Name!");
	console.log("Hello, " + samplePrompt);

let sampleNullPrompt = prompt("Don't Enter Anything!")
console.log(sampleNullPrompt); //prompt() returns an empty string when there is no input, or null if the user cancels the prompt

function printWelcomeMessage(){
	let firstName = prompt("Enter Your First Name");
	let lastName = prompt("Enter Your Last Name");
	alert("Thank You!");
	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to B248 class!");
}

printWelcomeMessage();

function getCourses(){
	let courses = ["Science","Englis","Math","Astrophysics"];
	console.log(courses);
}

getCourses();

function displayCarInfo() {
	console.log("Brand Toyota");
	console.log("Type Sedan");
	console.log("Price 1,500,000");
}

displayCarInfo();