console.log("Hello world")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){
	let name = prompt("What is your Name?");
	let age = prompt("How old are you?");
	let address = prompt("Where do you live?");
	alert("Thank You!");
	console.log("Hello, " + name);
	console.log("You are " + age + " years old");
	console.log("You live in " + address);
	console.log("My Favorite Music Artists:");
	console.log("1. Lil Pitchy");
	console.log("2. KSI");
	console.log("3. Boycce Avenue");
	console.log("4. Hunter Hayes");
	console.log("5. Arthur Nery");
	console.log("My Favorite Movies:");
	console.log("1. Interstellar");
	console.log("Rotten Tomatoes Rating: 72%");
	console.log("2. Top Gun: Maverick");
	console.log("Rotten Tomatoes Rating: 96%");
	console.log("3. John Wick: Chapter 3 – Parabellum");
	console.log("Rotten Tomatoes Rating: 89%");
	console.log("4. The Dark Knight");
	console.log("Rotten Tomatoes Rating: 94%");
	console.log("5. Harry Potter and the Prisoner of Azkaban");
	console.log("Rotten Tomatoes Rating: 90%");


}

printUsers();


let printFriends = function printFriendNames(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);